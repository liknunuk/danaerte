-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: kasertesmg0401
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `kas_aliran`
--

DROP TABLE IF EXISTS `kas_aliran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kas_aliran` (
  `nomor` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kodePosting` int(2) DEFAULT NULL,
  `uraian` varchar(30) DEFAULT NULL,
  `jumlah` int(8) unsigned DEFAULT '0',
  PRIMARY KEY (`nomor`),
  KEY `kas_kdpos` (`kodePosting`),
  CONSTRAINT `kas_kdpos` FOREIGN KEY (`kodePosting`) REFERENCES `kas_kodepos` (`nomor`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kas_aliran`
--

LOCK TABLES `kas_aliran` WRITE;
/*!40000 ALTER TABLE `kas_aliran` DISABLE KEYS */;
INSERT INTO `kas_aliran` VALUES (2,'2020-01-08 12:35:21',1,'Saldo Des 2019',1289000),(3,'2020-01-08 13:10:58',6,'Arisan a.n RT',50000),(4,'2020-01-08 13:28:41',4,'Bon . Dwi Agus',60000),(5,'2020-01-08 13:34:01',5,'Bon. Tambunan',60000),(6,'2020-01-08 13:38:52',2,'Kas. Jan 2020',65000),(7,'2020-01-08 13:47:08',3,'Tambahan kas',5000),(8,'2020-02-08 13:51:59',1,'Saldo januari 2020',1189000),(9,'2020-02-08 13:52:31',6,'Arisan a.n RT',50000),(10,'2020-02-17 04:52:15',2,'contoh',0);
/*!40000 ALTER TABLE `kas_aliran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kas_kodepos`
--

DROP TABLE IF EXISTS `kas_kodepos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kas_kodepos` (
  `nomor` int(2) NOT NULL AUTO_INCREMENT,
  `alur` enum('masuk','keluar','saldo') DEFAULT 'masuk',
  `nama` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kas_kodepos`
--

LOCK TABLES `kas_kodepos` WRITE;
/*!40000 ALTER TABLE `kas_kodepos` DISABLE KEYS */;
INSERT INTO `kas_kodepos` VALUES (1,'saldo','Saldo Bulan Lalu'),(2,'masuk','Kas Arisan'),(3,'masuk','Pemasukan Lain'),(4,'masuk','Pengembaian Bon'),(5,'keluar','Bon Arisan'),(6,'keluar','Pengeluaran Lain');
/*!40000 ALTER TABLE `kas_kodepos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saldoFix`
--

DROP TABLE IF EXISTS `saldoFix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saldoFix` (
  `pos` varchar(10) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jumlah` int(8) unsigned DEFAULT '0',
  UNIQUE KEY `pos` (`pos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saldoFix`
--

LOCK TABLES `saldoFix` WRITE;
/*!40000 ALTER TABLE `saldoFix` DISABLE KEYS */;
INSERT INTO `saldoFix` VALUES ('Kas','2020-01-08 07:24:29',0),('Jimpitan','2020-01-08 07:24:29',0),('Sosial','2020-01-08 07:24:29',0),('Simpin','2020-01-08 07:24:29',0);
/*!40000 ALTER TABLE `saldoFix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `viewAliran`
--

DROP TABLE IF EXISTS `viewAliran`;
/*!50001 DROP VIEW IF EXISTS `viewAliran`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `viewAliran` (
  `nomor` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `alur` tinyint NOT NULL,
  `nama` tinyint NOT NULL,
  `uraian` tinyint NOT NULL,
  `jumlah` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `viewAliran`
--

/*!50001 DROP TABLE IF EXISTS `viewAliran`*/;
/*!50001 DROP VIEW IF EXISTS `viewAliran`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`thebandars0404`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `viewAliran` AS select `kas_aliran`.`nomor` AS `nomor`,`kas_aliran`.`tanggal` AS `tanggal`,`kas_kodepos`.`alur` AS `alur`,`kas_kodepos`.`nama` AS `nama`,`kas_aliran`.`uraian` AS `uraian`,`kas_aliran`.`jumlah` AS `jumlah` from (`kas_aliran` join `kas_kodepos`) where (`kas_kodepos`.`nomor` = `kas_aliran`.`kodePosting`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-09 20:58:12
