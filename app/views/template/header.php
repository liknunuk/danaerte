<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?= $data['title']; ?></title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="<?=BASEURL;?>/img/logo-idi.png" width="16px" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap 4 CDN -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- jquery-ui -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= BASEURL .'css/danaerte.css'; ?>">
  <!-- Custom Script u/ restful API -->
  <!--script> var pusdata = "< ?=PUSDATA;?>"; var baseurl = "< ?=BASEURL;?>"; </script-->
</head>
<body>
<div class="container-fluid">

