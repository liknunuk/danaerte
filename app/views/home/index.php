<div class="row">
     
    <div class="col-md-12">
        <p class="h4">Laporan Kas Bulan Ini</p>
        <p class="h5">Saldo Terakhir</p>
        <table class="table table-sm">
            <thead>
               <tr>
                  <th>Uraian</th>
                  <th>Jumlah</th>
               </tr>
            </thead>
            <tbody>
            <?php foreach($data['aliran']['saldo'] AS $saldo ): ?>
                <tr>
                    <td><span class="flow"><?=$saldo['uraian'];?></span>
                    </td>
                    <td class='text-right'><?=number_format($saldo['jumlah'],0,',','.');?></td>
                </tr>
            <?php 
                $sa = $saldo['jumlah'];
                endforeach; 
            ?>
                
            </tbody>
        </table>
        <p class="h5">Pemasukan</p>
        <table class="table table-sm bg-lightgray">
            <thead>
               <tr>
                  <th>Uraian</th>
                  <th>Jumlah</th>
               </tr>
            </thead>
            <tbody>
            <?php 
                $tomas = 0;
                $namaPos = '';
                foreach($data['aliran']['masuk'] AS $masuk ): 
                if($masuk['nama'] !=$namaPos ) {
                    echo "<tr class='bg-success text-light'><td colspan='2'>".$masuk['nama']."</td></tr>";
                }
                $namaPos = $masuk['nama'];
            ?>
                <tr>
                    <td>
                        <span class="flow"><?=$masuk['uraian'];?></span>
                    </td>
                    <td class='text-right'><?=number_format($masuk['jumlah'],0,',','.');?></td>
                </tr>
            <?php 
                $tomas += $masuk['jumlah'];
                endforeach; 
            ?>
                <tr>
                    <td>Jumlah Pemasukan</td>
                    <td class="text-right"><?=number_format($tomas,0,',','.');?></td>
                </tr>
            </tbody>
        </table>

        <p class="h5">Pengeluaran</p>
        <table class="table table-sm">
            <thead>
               <tr>
                  <th>Uraian</th>
                  <th>Jumlah</th>
               </tr>
            </thead>
            <tbody>
            <?php 
                $tokel = 0;
                $namaPos='';
                foreach($data['aliran']['keluar'] AS $keluar ): 
                if($keluar['nama'] !=$namaPos ) {
                    echo "<tr class='bg-success text-light'><td colspan='2'>".$keluar['nama']."</td></tr>";
                }
                $namaPos = $keluar['nama'];
            ?>
                <tr>
                    <td>
                        <span class="flow"><?=$keluar['uraian'];?></span>
                    </td>
                    <td class='text-right'><?=number_format($keluar['jumlah'],0,',','.');?></td>
                </tr>
            <?php 
                $tokel += $keluar['jumlah'];
                endforeach; 
            ?>
            <tr>
                    <td>Jumlah Pengeluaran</td>
                    <td class="text-right"><?=number_format($tokel,0,',','.');?></td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p class="h2">SALDO AKHIR</p>
        <p class="h4 text-right bg-info px-3 py-2">
        <?php
            
            $ms = $tomas;
            $kl = $tokel;
            $saldoAkhir = $sa + $ms - $kl;
            echo number_format($saldoAkhir,0,',','.');
        ?>
        </p>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>