<div class="row">
    <div class="col-md-6">
    <p class="h4">Aliran Dana Kas RT</p>
        <form action="<?=BASEURL;?>Kas/setKas" method="post">
            <div class="form-group row">
                <label for="kas_posting" class="col-sm-4">Posting</label>
                <div class="col-sm-8">
                    <select name="kas_posting" id="kas_posting" class="form-control">
                    <?php foreach($data['kodePos'] AS $pos): ?>
                        <option value="<?=$pos['nomor'];?>"><?=$pos['nama'];?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="kas_uraian" class="col-sm-4">uraian</label>
                <div class="col-sm-8">
                    <input type="text" name="kas_uraian" id="kas_uraian" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="kas_jumlah" class="col-sm-4">Jumlah</label>
                <div class="col-sm-8">
                    <input type="number" name="kas_jumlah" id="kas_jumlah" class="form-control">
                </div>
            </div>

            <div class="form-group text-right">
                <button type="submit" id="kas_submit" class="btn btn-primary">Simpan</button>
            </div>
            <div class="form-group text-center">
                <?php Alert::sankil(); ?>
            </div>

        </form>
    </div>
    
    <div class="col-md-6">
        <p class="h4">Laporan Kas Bulan Ini</p>
        <p class="h5">Saldo Terakhir</p>
        <table class="table table-sm">
            <thead>
               <tr>
                  <th>Tanggal / Uraian</th>
                  <th>Jumlah</th>
               </tr>
            </thead>
            <tbody>
            <?php foreach($data['aliran']['saldo'] AS $saldo ): ?>
                <tr>
                    <td>
                        <span class="flow"><?=$saldo['tanggal'];?></span>
                        <span class="flow"><?=$saldo['uraian'];?></span>
                    </td>
                    <td class='text-right'><?=number_format($saldo['jumlah'],0,',','.');?></td>
                </tr>
            <?php 
                $sa = $saldo['jumlah'];
                endforeach; 
            ?>
                
            </tbody>
        </table>
        <p class="h5">Pemasukan</p>
        <table class="table table-sm bg-lightgray">
            <thead>
               <tr>
                  <th>Tanggal / Uraian</th>
                  <th>Jumlah</th>
               </tr>
            </thead>
            <tbody>
            <?php 
                $tomas = 0;
                foreach($data['aliran']['masuk'] AS $masuk ): 
            ?>
                <tr>
                    <td>
                        <span class="flow"><?=$masuk['tanggal'];?></span>
                        <span class="flow"><?=$masuk['uraian'];?></span>
                    </td>
                    <td class='text-right'><?=number_format($masuk['jumlah'],0,',','.');?></td>
                </tr>
            <?php 
                $tomas += $masuk['jumlah'];
                endforeach; 
            ?>
                <tr>
                    <td>Jumlah Pemasukan</td>
                    <td class="text-right"><?=number_format($tomas,0,',','.');?></td>
                </tr>
            </tbody>
        </table>

        <p class="h5">Pengeluaran</p>
        <table class="table table-sm">
            <thead>
               <tr>
                  <th>Tanggal / Uraian</th>
                  <th>Jumlah</th>
               </tr>
            </thead>
            <tbody>
            <?php 
                $tokel = 0;
                foreach($data['aliran']['keluar'] AS $keluar ): 
            ?>
                <tr>
                    <td>
                        <span class="flow"><?=$keluar['tanggal'];?></span>
                        <span class="flow"><?=$keluar['uraian'];?></span>
                    </td>
                    <td class='text-right'><?=number_format($keluar['jumlah'],0,',','.');?></td>
                </tr>
            <?php 
                $tokel += $keluar['jumlah'];
                endforeach; 
            ?>
            <tr>
                    <td>Jumlah Pengeluaran</td>
                    <td class="text-right"><?=number_format($tokel,0,',','.');?></td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p class="h2">SALDO AKHIR</p>
        <p class="h4 text-right bg-info px-3 py-2">
        <?php
            
            $ms = $tomas;
            $kl = $tokel;
            $saldoAkhir = $sa + $ms - $kl;
            echo number_format($saldoAkhir,0,',','.');
        ?>
        </p>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script> const baseurl='<?=BASEURL;?>';</script>
<script src="<?=BASEURL;?>js/index.js" type="text/javascript"></script>