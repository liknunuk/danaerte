<?php
class Model_kas
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function kodePosting(){
        $sql = "SELECT nomor,nama FROM kas_kodepos";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function setKas($data){
        
         $sql = "INSERT INTO kas_aliran SET kodePosting = :posting , uraian = :uraian , jumlah = :jumlah";

         $this->db->query($sql);
         $this->db->bind('posting',$data['kas_posting']);
         $this->db->bind('uraian',$data['kas_uraian']);
         $this->db->bind('jumlah',$data['kas_jumlah']);
         $this->db->execute();
         if( $data['kas_posting'] < 1 ){
             $this->updateSaldo($data['kas_posting'],$data['kas_jumlah']);
         }
         return $this->db->rowCount();
    }

    private function updateSaldo($pos,$jumlah){
        $alur = $this->alure($pos);
        if( $alur['kas'] == 'masuk' ){
            $sql = "UPDATE saldoFix SET jumlah = jumlah + $jumlah WHERE pos = 'Kas'";
        }elseif( $alur['kas'] == 'keluar' ){
            $sql = "UPDATE saldoFix SET jumlah = jumlah - $jumlah WHERE pos = 'Kas'";
        }else{ 
            return 0;
            exit();
        }

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->rowCount();
    }

    private function alure($pos){
        $sql = "SELECT alur kas FROM kas_kodepos WHERE nomor = :pos";
        $this->db->query($sql);
        $this->db->bind('pos',$pos);
        return $this->db->resultOne();
    }

    public function kasBulanIni(){
        $bulan = date('Y-m')."%";
        $saldo  = $this->saldoBulanIni($bulan); 
        $masuk  = $this->pemasukanBulanIni($bulan);
        $keluar = $this->pengeluaranBulanIni($bulan);

        return array('masuk'=>$masuk,'keluar'=>$keluar,'saldo'=>$saldo);
    }

    private function saldoBulanIni($bulan){
        $sql = "SELECT tanggal , uraian , jumlah FROM viewAliran WHERE tanggal LIKE :tanggal && alur = 'saldo' ORDER BY nomor";
        $this->db->query($sql);
        $this->db->bind('tanggal',$bulan);
        $this->db->execute();
        return $this->db->resultSet();
    }

    private function pemasukanBulanIni($bulan){
        $sql = "SELECT tanggal , nama , uraian , jumlah FROM viewAliran WHERE tanggal LIKE :tanggal && alur = 'masuk' ORDER BY nama,nomor";
        $this->db->query($sql);
        $this->db->bind('tanggal',$bulan);
        $this->db->execute();
        return $this->db->resultSet();
    }

    private function pengeluaranBulanIni($bulan){
        $sql = "SELECT tanggal , nama , uraian , jumlah FROM viewAliran WHERE tanggal LIKE :tanggal && alur = 'keluar' ORDER BY nama,nomor";
        $this->db->query($sql);
        $this->db->bind('tanggal',$bulan);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function entriSaldoAkhir(){
        $bulan = date('Y-m').'%';
        // $bulan ='2020-01%';
        $sql = "SELECT jumlah saldo FROM kas_aliran WHERE kodePosting = 1 && tanggal LIKE :bulan ";
        $this->db->query($sql);
        $this->db->bind('bulan',$bulan);
        return $this->db->resultOne();
    }

    public function saldoKas(){
        $sql = "SELECT jumlah kas FROM saldoFix WHERE pos='Kas'";
        $this->db->query($sql);
        return $this->db->resultOne();
    }

}
