<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $data['title'] = 'Kas RT';
    $data['kodePos'] = $this->model('Model_kas')->kodePosting();
    $data['aliran'] = $this->model('Model_kas')->kasBulanIni();
    $this->view('template/header',$data);
    $this->view('home/index',$data);
    $this->view('template/footer');
  }
}
