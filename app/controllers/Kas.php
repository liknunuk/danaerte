<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Kas extends Controller
{
  // method default
  public function index()
  {
    $data['title'] = 'Kas RT';
    $data['kodePos'] = $this->model('Model_kas')->kodePosting();
    $data['aliran'] = $this->model('Model_kas')->kasBulanIni();
    $this->view('template/header',$data);
    $this->view('Kas/index',$data);
    $this->view('template/footer');
  }

  public function setKas(){
      if($this->model('Model_kas')->setKas($_POST) > 0){
          Alert::setAlert("Berhasil Disimpan","Aliran Kas","success");
          header("Location:" . BASEURL . "Kas");
        }else{
            Alert::setAlert("Belum Tersimpan","Aliran Kas","danger");
            header("Location:" . BASEURL . "Kas");
      }
  }

  public function saldoKas(){
    $cekEntry = $this->model('Model_kas')->entriSaldoAkhir();
    if($cekEntry['saldo']==NULL){
      $saldo = $this->model('Model_kas')->saldoKas();
      echo $saldo['kas'];
    }else{
      echo "-1";
    }
  }
}
