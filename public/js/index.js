$(document).ready( function(){
    let pos = $('#kas_posting').val();
    if( pos == '1' ){
       cekSaldoKas();
    }

    $('#kas_posting').on('change',function(){
        if($(this).val() == '1' ){
            cekSaldoKas();
        }else{
            $('#kas_submit').show();
            $('#kas_uraian').prop('disabled',false);
            $('#kas_jumlah').prop('disabled',false);
        }
    })
})

function cekSaldoKas(){
    $.ajax({
        url: baseurl + 'Kas/saldoKas',
        success: function(saldo){
            if(saldo == '-1'){
                $('#kas_submit').hide();
                $('#kas_uraian').prop('disabled',true);
                $('#kas_jumlah').prop('disabled',true);
            }else{
                $('#kas_jumlah').val(saldo);
            }
        }
    })
}